import { Component, createRef } from '../../deps.ts';
import { DATABASE } from '../../app/app.tsx';

interface PictureAdderUIState {
  name: string;
  pictureBlob?: Blob;
  sounds: Array<string>;
  sound: string;
}

/** The value of the select item used when no sound is selected. */
const DEFAULT_SOUND_OPTION_VALUE = '-1';

export class PictureAdder extends Component<Record<never, never>, PictureAdderUIState> {
  state: PictureAdderUIState = {
    name: '',
    pictureBlob: undefined,
    sounds: [],
    sound: DEFAULT_SOUND_OPTION_VALUE,
  };

  #txtName = createRef<HTMLInputElement>();
  #filePicker = createRef<HTMLInputElement>();
  #canvas = createRef<HTMLCanvasElement>();

  render() {
    return (
      <>
        <ol>
          <li>
            Provide the <mark>name</mark> for the picture you are selecting.
          </li>
          <li>Optionally choose a sound to associate with the picture you are selecting.</li>
          <li>
            Press the <mark>Browse...</mark> button to select a picture.
          </li>
          <li>
            If everything looks/sounds good, press the <mark>Save</mark> button.
          </li>
          <li>
            Otherwise, change the name, choose another picture, or press <mark>Cancel</mark>.
          </li>
        </ol>
        <hr />
        <p>
          <label>Name</label>
          <input ref={this.#txtName} type='text' placeholder='name of picture' onInput={this.#onNameChange} value={this.state.name} />
        </p>
        <p>
          <label>Sound (optional)</label>
          <select onInput={this.#onSoundChange}>
            <option selected={this.state.sound === DEFAULT_SOUND_OPTION_VALUE} value={DEFAULT_SOUND_OPTION_VALUE}>(none)</option>
            {this.state.sounds.map((soundName) => <option selected={this.state.sound === soundName}>{soundName}</option>)}
          </select>
        </p>
        <p>
          <label>Choose Picture From Device</label>
          <input ref={this.#filePicker} type='file' accept='image/*' onChange={this.#onFileSelected} />
        </p>
        <p>
          <canvas ref={this.#canvas} hidden={this.state.pictureBlob === undefined}></canvas>
        </p>
        <p>
          <button onClick={this.#onSave} disabled={this.state.name.trim().length === 0 || this.state.pictureBlob === undefined}>Save</button>
          <button onClick={this.#onCancel}>Cancel</button>
        </p>
      </>
    );
  }

  #onNameChange = (e: Event) => {
    this.setState({ name: (e.target as HTMLInputElement).value.replaceAll(/[^a-z0-9A-Z]/g, '_') ?? '' });
  };

  #onSoundChange = (e: Event) => {
    this.setState({ sound: (e.target as HTMLOptionElement).value ?? DEFAULT_SOUND_OPTION_VALUE });
  };

  #onFileSelected = async (e: Event) => {
    const originalFile = this.#filePicker.current?.files![0]!;
    const reducedBitmap = await createImageBitmap(originalFile, { resizeHeight: 200, resizeQuality: 'pixelated' });

    const canvas = this.#canvas.current!;
    canvas.height = reducedBitmap.height;
    canvas.width = reducedBitmap.width;
    const ctx = canvas.getContext('bitmaprenderer')!;
    ctx.transferFromImageBitmap(reducedBitmap);

    this.setState({ pictureBlob: (await new Promise((resolve: BlobCallback) => canvas.toBlob(resolve)))! });
  };

  #onSave = async () => {
    await DATABASE.addPictureFile(`${this.state.name}.png`, this.state.pictureBlob!);

    if (this.state.sound !== DEFAULT_SOUND_OPTION_VALUE) {
      await DATABASE.addPictoThing({ picture: `${this.state.name}.png`, sound: this.state.sound });
    }

    this.#onCancel();
  };

  #onCancel = () => {
    this.setState({ name: '', sound: DEFAULT_SOUND_OPTION_VALUE, pictureBlob: undefined });
    this.#filePicker.current!.value = '';
  };

  #audioChanged = async () => {
    this.setState({ sounds: await DATABASE.getAllSounds() });
  };

  override async componentDidMount() {
    self.addEventListener('pictoReaderDatabaseSoundChanged', this.#audioChanged);
    await this.#audioChanged();
  }

  override componentWillUnmount() {
    self.removeEventListener('pictoReaderDatabaseSoundChanged', this.#audioChanged);
  }
}
