import { Component } from '../../deps.ts';
import { PictureAdder } from './PictureAdder.tsx';
import { PictureList } from './PictureList.tsx';

export class PicturePage extends Component {
  render() {
    return (
      <>
        <PictureList />
        <details>
          <summary>Add a new picture.</summary>
          <PictureAdder />
        </details>
      </>
    );
  }
}
