import { Component } from '../../deps.ts';
import { DATABASE } from '../../app/app.tsx';
import { PictoThing } from '../../services/database.ts';

interface PictureAndOptionalSoundArray {
  picture: string;
  sounds?: Array<string>;
}

interface PictureListUIState {
  pictures: Array<PictureAndOptionalSoundArray>;
}

export class PictureList extends Component<Record<never, never>, PictureListUIState> {
  state: PictureListUIState = { pictures: [] };
  #allPictureNames: Array<string> = [];
  #allPictoThings: Array<PictoThing> = [];

  render() {
    return (
      <section class='picturelist'>
        {this.state.pictures.map((pseudoPictoThing) => (
          <div>
            <img src={`pictures/${pseudoPictoThing.picture}`} title={pseudoPictoThing.picture} />
            <span>{pseudoPictoThing.picture}</span>
            {pseudoPictoThing.sounds?.map((s) => (
              <span>
                <button onClick={this.#onPlay} title={s}>▶</button>
                <audio src={`sounds/${s}`} preload='auto'></audio>
              </span>
            ))}
          </div>
        ))}
      </section>
    );
  }

  #onPlay(e: Event) {
    // TODO find the correct way to do this
    let x = e.target as Element;
    do {
      x = x.nextElementSibling!;
    } while (x.tagName !== 'AUDIO');

    (x as HTMLAudioElement).play();
  }

  #getPictureSoundMap(): Array<{ picture: string; sound?: string }> {
    return this.#allPictureNames.map((picture) => ({
      picture,
      sounds: this.#allPictoThings.filter((t) => t.picture === picture).map((t) => t.sound),
    }));
  }

  #pictureChanged = async () => {
    this.#allPictureNames = await DATABASE.getAllPictures();
    this.setState({ pictures: this.#getPictureSoundMap() });
  };

  #pictoThingChanged = async () => {
    this.#allPictoThings = await DATABASE.getAllPictoThings();
    this.setState({ pictures: this.#getPictureSoundMap() });
  };

  override async componentDidMount() {
    self.addEventListener('pictoReaderDatabasePictureChanged', this.#pictureChanged);
    self.addEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
    await this.#pictureChanged();
    await this.#pictoThingChanged();
  }

  override componentWillUnmount() {
    self.removeEventListener('pictoReaderDatabasePictureChanged', this.#pictureChanged);
    self.removeEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
  }
}
