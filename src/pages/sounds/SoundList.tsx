import { Component } from '../../deps.ts';
import { DATABASE } from '../../app/app.tsx';
import { PictoThing } from '../../services/database.ts';

interface SoundAndOptionalPictureArray {
  sound: string;
  pictures?: Array<string>;
}

interface SoundListUIState {
  sounds: Array<SoundAndOptionalPictureArray>;
}

export class SoundList extends Component<Record<never, never>, SoundListUIState> {
  state: SoundListUIState = { sounds: [] };
  #allSoundNames: Array<string> = [];
  #allPictoThings: Array<PictoThing> = [];

  render() {
    return (
      <section class='soundlist'>
        {this.state.sounds.map((pseudoPictoThing) => (
          <div>
            <button onClick={this.#onPlay}>▶</button>
            <span>{pseudoPictoThing.sound}</span>
            <audio src={`sounds/${pseudoPictoThing.sound}`} preload='auto'></audio>
            {pseudoPictoThing.pictures?.map((p) => <img src={`pictures/${p}`} title={p} />)}
          </div>
        ))}
      </section>
    );
  }

  #onPlay(e: Event) {
    // TODO find the correct way to do this
    let x = e.target as Element;
    do {
      x = x.nextElementSibling!;
    } while (x.tagName !== 'AUDIO');

    (x as HTMLAudioElement).play();
  }

  #getPictureSoundMap(): Array<SoundAndOptionalPictureArray> {
    return this.#allSoundNames.map((sound) => ({
      sound,
      pictures: this.#allPictoThings.filter((t) => t.sound === sound).map((t) => t.picture),
    }));
  }

  #audioChanged = async () => {
    this.#allSoundNames = await DATABASE.getAllSounds();
    this.setState({ sounds: this.#getPictureSoundMap() });
  };

  #pictoThingChanged = async () => {
    this.#allPictoThings = await DATABASE.getAllPictoThings();
    this.setState({ sounds: this.#getPictureSoundMap() });
  };

  override async componentDidMount() {
    self.addEventListener('pictoReaderDatabaseSoundChanged', this.#audioChanged);
    self.addEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
    await this.#audioChanged();
    await this.#pictoThingChanged();
  }

  override componentWillUnmount() {
    self.removeEventListener('pictoReaderDatabaseSoundChanged', this.#audioChanged);
    self.removeEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
  }
}
