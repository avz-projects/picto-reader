import { Component, createRef } from '../../deps.ts';
import { DATABASE } from '../../app/app.tsx';

interface SoundRecorderUIState {
  micstate: 'UNKNOWN' | 'REJECTED' | 'READY' | 'RECORDING' | 'RECORDED';
  name: string;
  pictures: Array<string>;
  picture: string;
}

/** The value of the select item used when no picture is selected. */
const DEFAULT_PICTURE_OPTION_VALUE = '-1';

export class SoundRecorder extends Component<Record<never, never>, SoundRecorderUIState> {
  state: SoundRecorderUIState = {
    micstate: 'UNKNOWN',
    name: '',
    pictures: [],
    picture: DEFAULT_PICTURE_OPTION_VALUE,
  };

  #txtName = createRef<HTMLInputElement>();
  #audio = createRef<HTMLAudioElement>();
  #mic?: MediaStream;
  #recorder?: MediaRecorder;
  #blob?: Blob;

  render() {
    return (
      <>
        <p hidden={this.state.micstate !== 'UNKNOWN'}>You must allow microphone access in order to add new audio using your microphone.</p>
        <p hidden={this.state.micstate !== 'REJECTED'}>Microphone access was rejeced. It must be enabled to record new audio.</p>
        <div hidden={this.state.micstate === 'UNKNOWN' || this.state.micstate === 'REJECTED'}>
          <ol>
            <li>
              Provide the <mark>name</mark> for the sound you are recording.
            </li>
            <li>Optionally choose a picture to associate with the sound you are recording.</li>
            <li>
              Press the <mark>🎤</mark> button to begin recording.
            </li>
            <li>Say the word.</li>
            <li>
              Press the <mark>⏹</mark> button to stop recording.
            </li>
            <li>
              Press the <mark>▶</mark> button to listen to your recording.
            </li>
            <li>
              If everything looks/sounds good, press the <mark>Save</mark> button.
            </li>
            <li>
              Otherwise, change the name, re-record a sound, or press <mark>Cancel</mark>.
            </li>
          </ol>
          <hr />
          <p>
            <label>Name</label>
            <input ref={this.#txtName} type='text' placeholder='name of sound' onInput={this.#onNameChange} value={this.state.name} />
          </p>
          <p>
            <label>Picture (optional)</label>
            <select onInput={this.#onPictureChange}>
              <option selected={this.state.picture === DEFAULT_PICTURE_OPTION_VALUE} value={DEFAULT_PICTURE_OPTION_VALUE}>(none)</option>
              {this.state.pictures.map((pictureName) => <option selected={this.state.picture === pictureName}>{pictureName}</option>)}
            </select>
          </p>
          <p>
            <label>Capture Audio From Microphone</label>
            <button onClick={this.#onStart} hidden={this.state.micstate === 'RECORDING'}>🎤</button>
            <button onClick={this.#onStop} hidden={this.state.micstate !== 'RECORDING'}>⏹</button>
            <button onClick={this.#onPlay} disabled={this.state.micstate !== 'RECORDED'}>▶</button>
            <audio ref={this.#audio} preload='auto'></audio>
          </p>
          <p>
            <button onClick={this.#onSave} disabled={this.state.name.trim().length === 0 || this.state.micstate !== 'RECORDED'}>Save</button>
            <button onClick={this.#onCancel}>Cancel</button>
          </p>
        </div>
      </>
    );
  }

  #onNameChange = (e: Event) => {
    this.setState({ name: (e.target as HTMLInputElement).value.replaceAll(/[^a-z0-9A-Z]/g, '_') ?? '' });
  };

  #onPictureChange = (e: Event) => {
    this.setState({ picture: (e.target as HTMLOptionElement).value ?? DEFAULT_PICTURE_OPTION_VALUE });
  };

  #onStart = () => {
    this.#recorder?.start();
    this.setState({ micstate: 'RECORDING' });
  };

  #onStop = () => {
    this.#recorder?.stop();
  };

  #onPlay = () => {
    this.#audio.current?.play();
  };

  #onSave = async () => {
    await DATABASE.addSoundFile(`${this.state.name}.ogg`, this.#blob!);

    if (this.state.picture !== DEFAULT_PICTURE_OPTION_VALUE) {
      await DATABASE.addPictoThing({ sound: `${this.state.name}.ogg`, picture: this.state.picture });
    }

    this.#onCancel();
  };

  #onCancel = () => {
    this.#blob = undefined;
    this.#txtName.current!.value = '';
    this.setState({ micstate: 'READY', picture: DEFAULT_PICTURE_OPTION_VALUE });
  };

  #pictureChanged = async () => {
    this.setState({ pictures: await DATABASE.getAllPictures() });
  };

  override async componentDidMount() {
    self.addEventListener('pictoReaderDatabasePictureChanged', this.#pictureChanged);
    await this.#pictureChanged();

    try {
      this.#mic = await navigator.mediaDevices.getUserMedia({ audio: true });

      this.#recorder = new MediaRecorder(this.#mic);

      this.#recorder.addEventListener('dataavailable', (event: BlobEvent) => {
        this.#blob = event.data;
        this.#audio.current!.src = window.URL.createObjectURL(this.#blob);
        this.setState({ micstate: 'RECORDED' });
      });

      this.setState({ micstate: 'READY' });
    } catch {
      console.log('TODO mic was rejected? could anything else cause this?', this.#mic);
      this.setState({ micstate: 'REJECTED' });
    }
  }

  override componentWillUnmount() {
    self.removeEventListener('pictoReaderDatabasePictureChanged', this.#pictureChanged);
  }
}
