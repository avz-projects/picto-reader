import { Component } from '../../deps.ts';
import { SoundList } from './SoundList.tsx';
import { SoundRecorder } from './SoundRecorder.tsx';

export class SoundPage extends Component {
  render() {
    return (
      <>
        <SoundList />
        <details>
          <summary>Add a new picture.</summary>
          <SoundRecorder />
        </details>
      </>
    );
  }
}
