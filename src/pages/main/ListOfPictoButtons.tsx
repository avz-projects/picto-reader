import { Component } from '../../deps.ts';
import { DATABASE } from '../../app/app.tsx';
import { PictoThing } from '../../services/database.ts';
import { PictoButton } from './PictoButton.tsx';

interface ListOfPictoButtonsUIState {
  data: Array<PictoThing>;
}

export class ListOfPictoButtons extends Component<Record<never, never>, ListOfPictoButtonsUIState> {
  state: ListOfPictoButtonsUIState = { data: [] };

  render() {
    return (
      <div class='pictolist'>
        {this.state.data.map((p) => <PictoButton sound={p.sound} picture={p.picture} />)}
      </div>
    );
  }

  #pictoThingChanged = async () => {
    this.setState({ data: await DATABASE.getAllPictoThings() });
  };

  override async componentDidMount() {
    self.addEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
    await this.#pictoThingChanged();
  }

  override componentWillUnmount() {
    self.removeEventListener('pictoReaderDatabasePictoThingChanged', this.#pictoThingChanged);
  }
}
