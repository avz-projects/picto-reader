import { PictoThing } from '../../services/database.ts';
import { useRef } from '../../deps.ts';

export function PictoButton(props: PictoThing) {
  const audio = useRef<HTMLAudioElement>(null);

  function playSound() {
    audio.current?.play();
  }

  return (
    <>
      <input type='image' src={`pictures/${props.picture}`} onClick={playSound} />
      <audio ref={audio} src={`sounds/${props.sound}`} preload='auto'></audio>
    </>
  );
}
