export { render, Component, createRef } from 'preact';
export { useState, useCallback, useRef } from 'preact/hooks';

export { Router } from 'preact-router'
export { Link } from 'preact-router/match'

export { default as localforage } from 'localforage'

// added to mache cache happy
import { JSX as _JSX } from 'preact/jsx-runtime'