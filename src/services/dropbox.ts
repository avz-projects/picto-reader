import { PictoThing } from './database.ts'
import localforage from 'localforage'

const DROPBOX_TOKEN_LOCAL_STORAGE_KEY = 'Picto Reader - Dropbox Token'
const API = 'https://api.dropboxapi.com'
const API_CONTENT = 'https://content.dropboxapi.com'
const CLIENT_ID = `dnmhmeogfmfvgqq`
const REDIRECT_URI = `${location.origin}${location.pathname}`.replace(/\/$/, '')
const CODE_VERIFIER = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
export const DROPBOX_AUTHORIZE_URL = `https://www.dropbox.com/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&code_challenge_method=plain&code_challenge=${CODE_VERIFIER}`

let TOKEN: string;

async function getToken(code: string) {
    const res = await fetch(`${API}/oauth2/token`, {
        method: 'POST',
        body: new URLSearchParams({
            code,
            grant_type: 'authorization_code',
            redirect_uri: REDIRECT_URI,
            // TODO this string should be more 'secure', i guess
            code_verifier: CODE_VERIFIER,
            client_id: CLIENT_ID
        }).toString(),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    });

    // status === 200   ->   token returned
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return (<{ access_token: string }>(await res.json())).access_token
}

/**
 * https://www.dropbox.com/developers/documentation/http/documentation#check-user
 * @param query The string to be sent to the API which is returned in the response.
 * @returns true if the request was successful, false otherwise
 */
async function postCheckUser(query = '🦃') {
    const res = await fetch(`${API}/2/check/user`, {
        method: 'POST',
        body: JSON.stringify({ query }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    const result: { result: string } = await res.json()

    return res.ok &&
        res.status === 200 &&
        result.result === query
}

export async function authenticate() {
    console.log('[DROPBOX] Begin authentication process.')

    TOKEN = await localforage.getItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY) || ''

    if (!TOKEN) {
        const authCode = location.search.replace(/^\?code=/, '')
        TOKEN = await getToken(authCode)
        await localforage.setItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY, TOKEN)
    }

    if (!(await postCheckUser())) {
        await localforage.removeItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY)
        throw new Error('[DROPBOX] Token is no good.  Try again.');
    }

    console.log('[DROPBOX] Successfully authenticated to Dropbox.')
}

/**
 * Retrieve the *database* file from dropbox and return the data it contains.
 *
 * @example
 * await getDataFile()  //  [ { sound: '...', picture: '...' }, ...]
 */
export async function getDataFile(): Promise<Array<PictoThing>> {
    return await (await getFile('/data.json')).json()
}

/**
 * Retrieve a *picture* file response from dropbox.
 *
 * @example
 * // `Response` of picture file request
 * const response = await getPictureResponse('foo.png')
 * // `Blob` of picture file
 * const blob = await response.blob()
 *
 *
 * @param filename - Filename of a picture to retrieve from Dropbox.  e.g., `foo.png`
 */
export async function getPictureResponse(filename: string): Promise<Response> {
    return await getFile(`/pictures/${filename}`)
}

/**
 * Retrieve a *sound* file response from dropbox.
 *
 * @example
 * // `Response` of sound file request.
 * const response = await getSoundResponse('foo.ogg')
 * // `Blob` of audio file
 * const blob = await response.blob()
 *
 * @param filename - Filename of a sound to retrieve from Dropbox.  e.g., `foo.ogg`
 */
export async function getSoundResponse(filename: string): Promise<Response> {
    return await getFile(`/sounds/${filename}`)
}

/**
 * @param path - Must be `'/data.json'`, `'/sounds/file.ext'`, or `'/pictures/file.ext'`
 * @returns Promise resolving to the response of the API call.  The response should have `.blob()` or `.json()` called on it.
 */
async function getFile(path: '/data.json' | string): Promise<Response> {

    // HACK figure out how SW token should be handled.
    TOKEN = await localforage.getItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY) || ''

    const res = await fetch(`${API_CONTENT}/2/files/download`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Dropbox-API-Arg': JSON.stringify({ path }),
        },
    });

    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return res
}

export async function addFile(path: string, body: string | Blob) {

    // TODO is path required to begin with /?

    const res = await fetch(`${API_CONTENT}/2/files/upload`, {
        method: 'POST',
        body,
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/octet-stream',
            'Dropbox-API-Arg': JSON.stringify({
                path,
                mode: 'overwrite',  // TODO should change to 'update' and figure out proper 'rev' stuff for robustness.  See https://www.dropbox.com/developers/documentation/http/documentation#files-upload
                autorename: false
            }),
        },
    });

    // status === 200   ->   file uploaded
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return await res.json()
}

/**
 * Get a list of all pictures or sounds.
 * @example
 * let x = await getFileList('/sounds')  // [ 'apple.ogg', 'car.ogg' ]
 *
 * @param path Either `'/pictures'` or `'/sounds'`.
 * @returns Array of strings of file names.  Empty array if no files exist.
 */
export async function getFileList(path: '/pictures' | '/sounds') {
    const res = await fetch(`${API}/2/files/list_folder`, {
        method: 'POST',
        body: JSON.stringify({ path, limit: 2000 }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return (<{ entries: Array<{ '.tag': string, name: string, path_display: string }> }>(await res.json())).entries.filter(e => e['.tag'] === 'file').map(e => e.name)
}
