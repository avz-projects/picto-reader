import { addFile, getDataFile, getFileList } from './dropbox.ts'

export interface PictoThing {
    sound: string;
    picture: string;
}

export class Database {

    #pictoThingChanged = new Event('pictoReaderDatabasePictoThingChanged', { bubbles: true, composed: true, cancelable: true })
    #soundChanged = new Event('pictoReaderDatabaseSoundChanged', { bubbles: true, composed: true, cancelable: true })
    #pictureChanged = new Event('pictoReaderDatabasePictureChanged', { bubbles: true, composed: true, cancelable: true })

    #pictoThings?: Array<PictoThing>
    #sounds?: Array<string>
    #pictures?: Array<string>

    async getAllPictoThings(): Promise<Array<PictoThing>> {
        if (this.#pictoThings === undefined) {
            try {
                this.#pictoThings = await getDataFile()
            } catch (e) {
                console.error('TODO assuming data.json does not exist therefore returning empty array.  Is that ok?', e)
                this.#pictoThings = []
            }
        }

        return [...this.#pictoThings]
    }

    async getAllSounds(): Promise<Array<string>> {
        if (this.#sounds === undefined) {
            try {
                this.#sounds = await getFileList('/sounds')
            } catch (e) {
                console.error('TODO what happened?', e)
                this.#sounds = []
            }
        }

        return [...this.#sounds]
    }

    async getAllPictures(): Promise<Array<string>> {
        if (this.#pictures === undefined) {
            try {
                this.#pictures = await getFileList('/pictures')
            } catch (e) {
                console.error('TODO what happened?', e)
                this.#pictures = []
            }
        }

        return [...this.#pictures]
    }

    async addPictoThing(p: PictoThing) {
        if (this.#pictoThings === undefined) {
            await this.getAllPictoThings()
        }

        this.#pictoThings!.push(p)
        const retVal = await addFile('/data.json', JSON.stringify(this.#pictoThings))
        dispatchEvent(this.#pictoThingChanged)
        return retVal
    }

    async addSoundFile(name: string, file: Blob) {
        if (this.#sounds === undefined) {
            await this.getAllSounds()
        }

        this.#sounds!.push(name)
        const retVal = await addFile(`/sounds/${name}`, file)
        dispatchEvent(this.#soundChanged)
        return retVal
    }

    async addPictureFile(name: string, file: Blob) {
        if (this.#pictures === undefined) {
            await this.getAllPictures()
        }

        this.#pictures!.push(name)
        const retVal = await addFile(`/pictures/${name}`, file)
        dispatchEvent(this.#pictureChanged)
        return retVal
    }
}

declare global {
    interface WindowEventMap {
        'pictoReaderDatabasePictoThingChanged': 'pictoReaderDatabasePictoThingChanged'
        'pictoReaderDatabaseSoundChanged': 'pictoReaderDatabaseSoundChanged'
        'pictoReaderDatabasePictureChanged': 'pictoReaderDatabasePictureChanged'
    }
}
