/// <reference lib='webworker' />
import { getPictureResponse, getSoundResponse } from './services/dropbox.ts';
import json from './version.json' assert { type: 'json' };

type SWMessageData = { action: 'skipWaiting' }

interface PictoReaderExtendableMessageEvent extends ExtendableMessageEvent {
    data: SWMessageData
}

// Read and understand the following:
// https://developer.chrome.com/docs/workbox/service-worker-lifecycle/

const sw = self as unknown as ServiceWorkerGlobalScope

const cacheName = `picto-reader-app-${json.version}`
const PICTURE_CACHE = 'picto-reader-pictures'
const SOUND_CACHE = 'picto-reader-sounds'

const contentToCache = [
    '/picto-reader/',
    '/picto-reader/icon-192x192.png',
    '/picto-reader/icon-512x512.png',
    '/picto-reader/icon.svg',
    '/picto-reader/index.html',
    '/picto-reader/main.css',
    '/picto-reader/main.js',
    '/picto-reader/manifest.webmanifest'
]

sw.addEventListener('message', (e: PictoReaderExtendableMessageEvent) => {
    if (e.data.action === 'skipWaiting') {
        sw.skipWaiting()
    }
})

sw.addEventListener('install', (e: ExtendableEvent) => {
    e.waitUntil((async () => {
        const cache = await caches.open(cacheName);
        return cache.addAll(contentToCache);
    })())
})

sw.addEventListener('activate', (e: ExtendableEvent) => {
    e.waitUntil((async () => {
        for (const key of await caches.keys()) {
            // Delete old 'app' caches only.
            if (key !== cacheName && key !== PICTURE_CACHE) {
                await caches.delete(key)
            }
        }
    })())
})

sw.addEventListener('fetch', (e: FetchEvent) => {
    if (e.request.url.indexOf('/picto-reader/pictures/') >= 0) {
        e.respondWith((async () => {
            const cache = await caches.open(PICTURE_CACHE)
            const cachedResponse = await cache.match(e.request, { ignoreVary: true })
            if (cachedResponse) {
                return cachedResponse
            } else {
                const response = await getPictureResponse(e.request.url.substring(e.request.url.lastIndexOf('/') + 1))
                const clonedResponse = response.clone()

                await cache.put(
                    e.request,
                    new Response(clonedResponse.body, {
                        headers: {
                            'Content-Type': 'image/png',
                            'Content-Length': clonedResponse.headers.get('Content-Length') ?? ''
                        }
                    })
                )

                return response
            }
        })())
    } else if (e.request.url.indexOf('/picto-reader/sounds/') >= 0) {
        e.respondWith((async () => {
            const cache = await caches.open(SOUND_CACHE)
            const cachedResponse = await cache.match(e.request, { ignoreVary: true })
            if (cachedResponse) {
                return cachedResponse
            } else {
                const response = await getSoundResponse(e.request.url.substring(e.request.url.lastIndexOf('/') + 1))
                const clonedResponse = response.clone()

                await cache.put(
                    e.request,
                    new Response(clonedResponse.body, {
                        headers: {
                            'Content-Type': 'audio/ogg',
                            'Content-Length': clonedResponse.headers.get('Content-Length') ?? ''
                        }
                    })
                )

                return response
            }
        })())
    } else {
        e.respondWith((async () => {
            const cache = await caches.open(cacheName)
            const cachedResponse = await cache.match(e.request, { ignoreVary: true })
            return cachedResponse || await fetch(e.request)
        })())
    }
})
