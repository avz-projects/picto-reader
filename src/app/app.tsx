import { Component, Link, Router } from '../deps.ts';
import { MyFooter } from './footer.tsx';
import { DropboxAuthEventType, DropboxUI } from './DropboxUI.tsx';
import { Database, PictoThing } from '../services/database.ts';
import { MainPage } from '../pages/main/MainPage.tsx';
import { SoundPage } from '../pages/sounds/SoundPage.tsx';
import { PicturePage } from '../pages/pictures/PicturePage.tsx';

interface AppState {
  data: Array<PictoThing>;
  authState: DropboxAuthEventType | 'UNKNOWN';
}

/** TODO Rename. Ask if this is best service/DI approach. */
export const DATABASE = new Database();

export class App extends Component {
  state: AppState = { data: [], authState: 'UNKNOWN' };

  #dropboxChange = (state: DropboxAuthEventType) => this.setState({ authState: state });

  render() {
    switch (this.state.authState) {
      case 'AUTHENTICATED':
        return (
          <>
            <header>
              <nav>
                <Link activeClassName='current' href='/picto-reader/'>Main</Link>
                <Link activeClassName='current' href='/picto-reader/pictures'>Pictures</Link>
                <Link activeClassName='current' href='/picto-reader/sounds'>Sounds</Link>
              </nav>
              <h1>Picto Reader</h1>
              <p>Touch the 🖼. Play the 🔊. 🔁.</p>
            </header>
            <main>
              <Router>
                <MainPage default />
                <PicturePage path='/picto-reader/pictures' />
                <SoundPage path='/picto-reader/sounds' />
              </Router>
            </main>
            <MyFooter />
          </>
        );
      case 'UNAUTHENTICATED':
      case 'WAITING':
      case 'UNKNOWN':
        return <DropboxUI onChange={this.#dropboxChange} />;
    }
  }
}
