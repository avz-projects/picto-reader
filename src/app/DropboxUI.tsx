import { Component } from '../deps.ts';
import { authenticate, DROPBOX_AUTHORIZE_URL } from '../services/dropbox.ts';

export type DropboxAuthEventType = 'WAITING' | 'AUTHENTICATED' | 'UNAUTHENTICATED';

interface DropboxUIState {
  authstate: 'UNKNOWN' | DropboxAuthEventType;
}

interface DropboxUIProps {
  onChange: (state: DropboxAuthEventType) => void;
}

export class DropboxUI extends Component<DropboxUIProps, Record<never, never>> {
  state: DropboxUIState = { authstate: 'UNKNOWN' };

  override async componentDidMount() {
    try {
      this.setState({ authstate: 'WAITING' });
      this.props.onChange('WAITING');
      await authenticate();
      this.setState({ authstate: 'AUTHENTICATED' });
      this.props.onChange('AUTHENTICATED');
    } catch (_error) {
      console.log('[BROWSER] exception from authentication attempt.  User will need to authenticate with dropbox.');
      this.setState({ authstate: 'UNAUTHENTICATED' });
      this.props.onChange('UNAUTHENTICATED');
    }
  }

  render() {
    switch (this.state.authstate) {
      case 'AUTHENTICATED':
        return <div>Successfully authenticated with Dropbox.</div>;
      case 'UNAUTHENTICATED':
        return <a href={DROPBOX_AUTHORIZE_URL}>Authenticate with Dropbox</a>;
      case 'WAITING':
        return <div>Checking Dropbox authentication...</div>;
      case 'UNKNOWN':
        return <div>{this.state.authstate}</div>;
    }
  }
}
