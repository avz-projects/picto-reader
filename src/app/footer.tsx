import { Component } from '../deps.ts';
import json from '../version.json' assert { type: 'json' };

export class MyFooter extends Component {
  render() {
    return (
      <footer>
          <p>Picto Reader - {json.version}</p>
          <p>Commit: <a href={`https://gitlab.com/avz-projects//picto-reader/-/commit/${json.commit}`}>{json.commit.substring(0, 8)}</a></p>
          <p>Build: <a href={`https://gitlab.com/avz-projects//picto-reader/-/pipelines/${json.pipeline}`}>{json.build}</a></p>
      </footer>
    );
  }
}
