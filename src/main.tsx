import { render } from './deps.ts';
import { App } from './app/app.tsx';
import json from './version.json' assert { type: 'json' };

const reg = await navigator.serviceWorker.register('sw.js', { type: 'module' });

reg.addEventListener('updatefound', () => {
  const newWorker = reg.installing;
  newWorker?.addEventListener('statechange', () => {
    if (newWorker.state === 'installed' && navigator.serviceWorker.controller) {
      // Make sure the message matches the expected type in sw.ts.  See `SWMessageData`.
      newWorker.postMessage({ action: 'skipWaiting' });
    }
  });
});

navigator.serviceWorker.addEventListener('controllerchange', () => window.location.reload());

document.title = `Picto Reader - ${json.version}`;

render(<App />, document.body);
