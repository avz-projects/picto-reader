# Picto Reader

TODO

## Development

⚠ This info is for `picto-reader` developers only.

### Local dev

- Use `Live Preview: Start Server` option in VS Code.
- Navigate to the `picto-reader/` directory.
- Run `deno task build` to update the app.

### Work Item Process

- [GitLab] choose issue to work on
- [GitLab] assign issue to self
- [GitLab] click `Create merge request (and branch)` button
- [GitLab] assign MR to self
- [GitLab] click `Create merge request` button
- [desktop] `git checkout master`
- [desktop] `git pull`
- [desktop] `git checkout <issue #><tab>`
- [desktop] DO WORK
- [desktop] update `CHANGELOG.md` -> - `[ISSUE TITLE](avz-projects/picto-reader#ISSUE_NUBMER)`
- [desktop] commit changes
- [desktop] push changes
- [GitLab] review?/merge MR
- [desktop] `git checkout master`
- [desktop] `git pull --prune`
- [desktop] `git branch -D <issue #><tab>`
- [desktop] `git branch -a`

### Creating releases

- Follow the **Work Item Process** above.  Make the following code changes.
  - `src/version.json` - set version appropriately
  - `CHANGELOG.md` - finalize, set date, set version
- The app will deploy when the MR is merged and the master branch builds.

### Updating Dependencies

Dependencies are managed in `import_map.json` and `src/deps.ts`.
Make changes and run:

```shell
deno task update-cache
```

### Optional `master` alias

If you want to use `master` instead of `main`

```shell
git symbolic-ref refs/heads/master   refs/heads/main
```
