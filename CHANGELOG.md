# 1.16.0 (2022-10-21)

- [automatically update when updated version available](avz-projects/picto-reader#46)

# 1.15.0 (2022-10-21)

- [no special characters in picture/sound names](avz-projects/picto-reader#45)

# 1.14.0 (2022-10-20)

- [Display Functional Buttons on Main Page](avz-projects/picto-reader#37)

# 1.13.0 (2022-10-20)

- [Link Pictures and Audios](avz-projects/picto-reader#44)

# 1.12.0 (2022-10-19)

- [cache audio and pictures from dropbox](avz-projects/picto-reader#39)

# 1.11.0 (2022-10-14)

- [Display images from Dropbox in the Pictures page](avz-projects/picto-reader#33)

# 1.10.0 (2022-10-14)

- [Reduce the size of pictures](avz-projects/picto-reader#41)

# 1.9.0 (2022-10-12)

- [choose picture from device](avz-projects/picto-reader#26)

# 1.8.0 (2022-10-12)

- [Display audios from Dropbox in the Sounds page](avz-projects/picto-reader#34)

# 1.7.0 (2022-10-07)

- [v1.7 - record audio using microphone](avz-projects/picto-reader#27)

# 1.6.0 (2022-10-06)

- [v1.6 - Add Three Empty Pages for Button, Audio, and Image Maintenance](avz-projects/picto-reader#32)

# 1.5.0 (2022-09-30)

- [V1.5 - Use DropBox Instead of Local Storage for PictoThings](avz-projects/picto-reader#31)

# 1.4.0 (2022-09-30)

- [V1.4 - Connect App to DropBox PictoReader-Dev App](avz-projects/picto-reader#30)

# 1.3.0 (2022-09-27)

- [UI and storage for dynamically adding PictoButtons](avz-projects/picto-reader#25)

# 1.2.0 (2022-07-14)

- [Create a button on main page with provided sample image and audio](avz-projects/picto-reader#18)

# 1.1.0 (2022-07-14)

- [update title in index.html](avz-projects/picto-reader#6)
- [remove peg-path.ts](avz-projects/picto-reader#9)
- [update description meta data](avz-projects/picto-reader#5)
- [update deno version](avz-projects/picto-reader#11)
- [update preact version](avz-projects/picto-reader#10)
- [update app icon](avz-projects/picto-reader#4)
- [implement deno caching](avz-projects/picto-reader#8)
- [update readme](avz-projects/picto-reader#7)
- [fix build job cache usage](avz-projects/picto-reader#22)
